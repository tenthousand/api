<?php

use yii\db\Migration;

class m170104_112255_create_doing extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('doings', [
            'id'=>  $this->primaryKey(),
            'user_ref' => $this->integer()->notNull(),
            'title' => $this->string(),
            'ideal_time' => $this->integer(),
            'period'=>  $this->integer(),
        ]);
        $this->addForeignKey('user_doings', 'doings', 'user_ref', 'users', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('doings');
    }
}
