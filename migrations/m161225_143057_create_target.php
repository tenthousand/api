<?php

use yii\db\Migration;

class m161225_143057_create_target extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('dict_target_type', [
            'id' => $this->primaryKey(),
            'target_type_name' => $this->string()->notNull()
        ]);
        $this->batchInsert('dict_target_type', [
            'id', 'target_type_name'
        ], [
            [0,'Основная цель'],
            [1,'Глобальная цель'],
            [2,'Долгосрочная цель'],
            [3,'Краткосрочная цель']
        ]);
        
        $this->createTable('targets', [
            'id' => $this->primaryKey(),
            'target_name' => $this->string()->notNull(),
            'description' => $this->text(),
            'parent_ref' => $this->integer(),
            'user_ref' => $this->integer()->notNull(),
            'target_type_ref' => $this->integer()->notNull(),
            'deadline' => $this->integer()
        ]);
        $this->addForeignKey('target_parent', 'targets', 'parent_ref', 'targets', 'id', 'CASCADE');
        $this->addForeignKey('target_user', 'targets', 'user_ref', 'users', 'id', 'CASCADE');
        $this->addForeignKey('target_type', 'targets', 'target_type_ref', 'dict_target_type', 'id', 'RESTRICT');
    }

    public function safeDown()
    {
        $this->dropTable('targets');
        $this->dropTable('dict_target_type');
    }
}
