<?php

use yii\db\Migration;

class m161219_154336_create_user extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'email' => $this->string(),
            'password' => $this->string(200),
            'salt' => $this->string(10),
            'registration_date' => $this->integer(),
            'email_confirmed' => $this->boolean()
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('users');
    }
}
