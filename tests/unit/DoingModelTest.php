<?php

use app\controllers\SiteController;
use app\models\DoingModel;
use app\models\UserModel;
use Codeception\TestCase\Test;
use yii\web\Application;

require_once __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';

class DoingModelTest extends Test
{
    /**
     * @var UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $config = require(__DIR__ . '/../../config/console.php');
        (new Application($config));
    }

    protected function _after()
    {
    }

    // tests
    public function testCreateEditDelete()
    {
        $siteController = new SiteController('Site', '');
        $user = UserModel::find()->one();
        SiteController::setUser($user);

        $action = [
            'method' => 'doing/create',
            'body' => [
                'title' => 'Тестовое дело',
                'idealTime' => 600,
                'period' => 1
            ],
        ];
        $result = $siteController->controllerAction($action);
        $this->assertNotEmpty($result['id'],print_r($result,1));
        $id = $result['id'];

        $action = [
            'method' => 'doing/get',
            'body' => [
                'id' => $id
            ]
        ];
        $result = $siteController->controllerAction($action);
        $this->assertEquals('Тестовое дело', $result['title'],print_r($result,1));
        
        $action = [
            'method' => 'doing/edit',
            'body' => [
                'id' => $id,
                'idealTime' => 800
            ]
        ];
        $result = $siteController->controllerAction($action);
        $this->assertEquals(800, $result['idealTime'], print_r($result,1));
        
        
        $action = [
            'method' => 'doing/delete',
            'body' => [
                'id' => $id
            ]
        ];
        $result = $siteController->controllerAction($action);
        $doing = DoingModel::find()
                ->where(['id'=>$id])
                ->one();
        $this->assertEmpty($doing,print_r($result,1));

        /* @var $doings DoingModel[] */
        $doings = DoingModel::findAll(['user_ref'=>$user->id,'title'=>'Тестовое дело']);
        foreach ($doings as $doing){
            $doing->delete();
        }
        
    }
}