<?php

use app\controllers\SiteController;
use Codeception\TestCase\Test;

require_once __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';

class UserModelTest extends Test
{
    /**
     * @var UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $config = require(__DIR__ . '/../../config/console.php');
        (new yii\console\Application($config));
    }

    protected function _after()
    {
    }

    // tests
    public function testMe()
    {
        $siteController = new SiteController('Site','');
        $email = time().'@node.com';
        $pass = '1234567890';
        $data = [
            'email' => $email,
            'password' => $pass
        ];
        $action = ['method' => 'user/login', 'body' => $data];
        $result = $siteController::controllerAction($action);
        $this->assertNotEmpty($result['token']);
        
        $user = \app\models\UserModel::find()
                ->where(['email'=>$email])
                ->one();
        $user->delete();        
    }
}