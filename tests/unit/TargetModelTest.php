<?php

use app\controllers\SiteController;
use app\models\TargetModel;
use app\models\UserModel;
use Codeception\TestCase\Test;
use yii\console\Application;

require_once __DIR__ . '/../../vendor/yiisoft/yii2/Yii.php';

class TargetModelTest extends Test {

    /**
     * @var UnitTester
     */
    protected $tester;

    protected function _before() {
        $config = require(__DIR__ . '/../../config/console.php');
        (new Application($config));
    }

    protected function _after() {
        
    }

    // tests
    public function testCreateAndDelete() {
        $siteController = new SiteController('Site', '');
        $user = UserModel::find()->one();
        SiteController::setUser($user);

        $action = [
            'method' => 'target/create',
            'body' => [
                'target_name' => 'Чего-то хочется'
            ],
        ];
        $result = $siteController->controllerAction($action);
        $this->assertNotEmpty($result['id']);
        $id = $result['id'];

        $action = [
            'method' => 'target/list',
            'body' => [
                'parent_ref' => $id
            ]
        ];
        $result = $siteController->controllerAction($action);
        $this->assertEquals(0, count($result));
        
        $action = [
            'method' => 'target/delete',
            'body' => [
                'id' => $id
            ]
        ];
        $result = $siteController->controllerAction($action);
        $target = TargetModel::find()
                ->where(['id'=>$id])
                ->one();
        $this->assertEmpty($target);
    }

}
