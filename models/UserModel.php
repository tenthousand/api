<?php

namespace app\models;

use app\components\Jwt;
use app\components\Utility;
use Yii;
use yii\db\ActiveRecord;

require_once __DIR__.'/../components/Encryption/Jwt.php';

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $salt
 * @property integer $registration_date
 * @property boolean $email_confirmed
 */
class UserModel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['registration_date'], 'integer'],
            [['email_confirmed'], 'boolean'],
            [['email'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 255],
            [['password'], 'string', 'max' => 200],
            [['salt'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'name' => 'Name',
            'password' => 'Password',
            'salt' => 'Salt',
            'registration_date' => 'Registration Date',
            'email_confirmed' => 'Email Confirmed',
        ];
    }
    
    public function __set($name, $value) {
        switch ($name){
            case 'password':
                $this->salt = Utility::generateRandomString(10);
                $hash = hash_hmac('sha256', $value.$this->salt, Utility::getPassKey());
                parent::__set($name, $hash);
                break;
            case 'salt':
                break;
            default :
                parent::__set($name, $value);
                break;
        }
    }

    public function checkPassword($password){
        $hash = hash_hmac('sha256', $password.$this->salt, Utility::getPassKey());
        return $hash === $this->password;
    }
    
    public static function create($data){
        $user = new UserModel([
            'email' => $data['email'],
            'password' => $data['password'],
            'registration_date' => time(),
            'email_confirmed' => FALSE
        ]);
        $user->save();
        return $user;
    }

    public static function login($data){
        $data['email'] = strtolower($data['email']);
        $user = self::find()
                ->where(['email'=>$data['email']])
                ->one();
        if($user){
            if(!$user->checkPassword($data['password'])){
                Yii::$app->response->setStatusCode(401);
                return ['error'=>'Not correct password'];
            }
        }else{
            $user = UserModel::create($data);
        }
        return ['token'=>$user->token];
    }
    
    public function getToken(){
        $payload = [
            'email' => $this->email,
            'name' => $this->name,
            'sub' => $this->id,
            'iat' => time(),
            'exp' => time() + (2*7*24*60*60)
        ];
        return ( new Jwt())->encode($payload, Utility::getAppWeb()['token_secret']);
    }

    public static function getUserFromPayload($payload) {
        $user = self::find()
                ->where(['email'=>$payload['email']])
                ->one();
        return $user;
    }

    public function getInfo(){
        return [
            'id' => $this->id,
            'email' => $this->email,
            'name' => $this->name,

            ];
    }
}
