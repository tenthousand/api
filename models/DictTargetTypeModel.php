<?php

namespace app\models;

use Yii;

/**
 * Класс "Справочник типов целей".
 *
 * @property integer $id
 * @property string $target_type_name
 */
class DictTargetTypeModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dict_target_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['target_type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'target_type_name' => 'Target Type Name',
        ];
    }

}
