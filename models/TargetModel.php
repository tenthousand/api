<?php

namespace app\models;

use app\controllers\SiteController;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "targets".
 *
 * @property integer $id
 * @property string $target_name
 * @property string $description
 * @property integer $parent_ref
 * @property integer $user_ref
 * @property integer $target_type_ref
 * @property integer $deadline
 *
 * @property DictTargetTypeModel $targetType
 * @property TargetModel $parent
 * @property TargetModel[] $children
 * @property UserModel $user
 */
class TargetModel extends ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'targets';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['target_name', 'user_ref', 'target_type_ref'], 'required'],
            [['description'], 'string'],
            [['parent_ref', 'user_ref', 'target_type_ref', 'deadline'], 'integer'],
            [['target_name'], 'string', 'max' => 255],
            [['target_type_ref'], 'exist', 'skipOnError' => true, 'targetClass' => DictTargetTypeModel::className(), 'targetAttribute' => ['target_type_ref' => 'id']],
            [['parent_ref'], 'exist', 'skipOnError' => true, 'targetClass' => TargetModel::className(), 'targetAttribute' => ['parent_ref' => 'id']],
            [['user_ref'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::className(), 'targetAttribute' => ['user_ref' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'target_name' => 'Target Name',
            'description' => 'Description',
            'parent_ref' => 'Parent Ref',
            'user_ref' => 'User Ref',
            'target_type_ref' => 'Target Type Ref',
            'deadline' => 'Deadline',
        ];
    }

    public static function create($data) {
        $target = new TargetModel([
            'target_name' => $data['target_name'],
            'description' => empty($data['description']) ? '' : $data['description'],
            'parent_ref' => empty($data['parent_ref']) ? null : $data['parent_ref'],
            'user_ref' => SiteController::$user->id,
            'deadline' => empty($data['deadline']) ? mktime(0, 0, 0, date('n'), date('j'), date('Y') + 1) : $data['deadline']
        ]);
        $target->save();
        return $target;
    }

    public function __set($name, $value) {
        switch ($name) {
            case 'parent_ref':
                parent::__set($name, $value);
                if (empty($value)) {
                    $this->target_type_ref = 0;
                } else {
                    $$this->target_type_ref = $this->parent->target_type_ref + 1;
                }
                break;
            default:
                parent::__set($name, $value);
        }
    }

    /**
     * @return ActiveQuery
     */
    public function getTargetType() {
        return $this->hasOne(DictTargetTypeModel::className(), ['id' => 'target_type_ref']);
    }

    /**
     * @return ActiveQuery
     */
    public function getParent() {
        return $this->hasOne(TargetModel::className(), ['id' => 'parent_ref']);
    }

    /**
     * @return ActiveQuery
     */
    public function getChildren() {
        return $this->hasMany(TargetModel::className(), ['parent_ref' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(UserModel::className(), ['id' => 'user_ref']);
    }

    public function getInfo() {
        return [
            'id' => $this->id,
            'target_name' => $this->target_name,
            'description' => $this->description,
            'parent_ref' => $this->parent_ref,
            'user_ref' => $this->user_ref,
            'target_type' => $this->targetType->target_type_name,
            'deadline' => $this->deadline,
        ];
    }

}
