<?php

namespace app\models;

use app\controllers\SiteController;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Дела.
 *
 * @property integer $id
 * @property integer $user_ref
 * @property string $title
 * @property integer $ideal_time
 * @property integer $period
 *
 * @property UserModel $user
 * $property info
 */
class DoingModel extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_ref'], 'required'],
            [['user_ref', 'ideal_time', 'period'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['user_ref'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::className(), 'targetAttribute' => ['user_ref' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_ref' => 'User Ref',
            'title' => 'Title',
            'ideal_time' => 'Ideal Time',
            'period' => 'Period',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(UserModel::className(), ['id' => 'user_ref']);
    }
    
    public static function create($data){
        $doing = new DoingModel([
            'user_ref' => SiteController::$user->id,
            'title' => $data['title'],
            'ideal_time' => $data['idealTime'],
            'period' => $data['period']
        ]);
        $doing->save();
        return $doing;
    }
    
    public function edit($data){
        if(!empty($data['title'])){
            $this->title = $data['title'];
        }
        if(!empty($data['idealTime'])){
            $this->ideal_time = $data['idealTime'];
        }
        if(!empty($data['period'])){
            $this->period = $data['period'];
        }
    }
    
    public function checkUser(){
        return $this->user_ref == SiteController::$user->id;
    }
    
    public function getInfo(){
        return [
            'id' => $this->id,
            'title' => $this->title,
            'idealTime' => $this->ideal_time,
            'period' => $this->period
        ];
    }
}
