<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'tenthousand-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['gii'],
    'defaultRoute' => 'site/facade',
    'components' => [
        'urlManager' => [
            'scriptUrl' => '/index.php',
            'enablePrettyUrl' => true,			//false => index.php?r=site%2Fnewpage, true => index.php/site/newpage
            'enableStrictParsing' => false,
            'showScriptName' => true,
            'rules' => [
                'POST <controller:\w+>s' => '<controller>/create',
                '<controller:\w+>/<action:\w+>/<id:\w+>'=>'<controller>/<action>',
                'PUT <controller:\w+>/<id:\d+>'    => '<controller>/update',
                'DELETE <controller:\w+>/<id:\d+>' => '<controller>/delete',
                '<controller:\w+>/<id:\d+>'        => '<controller>/view',
                ]
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '8N3Mrwt_SseIhnJtsYE4-MQ9G_fUrVKW',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
//        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
