<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use app\models\TargetModel;

/**
 * Контроллер целей
 *
 * @author Виктор
 */
class TargetController {

    public function actionCreate($data) {
        $target = TargetModel::create($data);
        return ['id' => $target->id];
    }

    /**
     * 
     * @param [] $data ['id']
     */
    public function actionDelete($data) {
        $target = TargetModel::find()
                ->where(['id' => $data['id']])
                ->one();
        if (!empty($target)) {
            if (SiteController::$user->id != $target->user_ref) {
                return ['error' => 'Forbidden'];
            }
            $target->delete();
        }
        return ['result' => 'Ok'];
    }

    public function actionList($data) {
        $targets = TargetModel::find()
                ->where([
                    'parent_ref' => empty($data['parent_ref']) ? null : $data['parent_ref'],
                    'user_ref' => SiteController::$user->id
                ])
                ->all();
        $result = [];
        foreach ($targets as $target) {
            $result[] = $target->info;
        }
        return $result;
    }
    
    public function actionEdit($data) {
        /* @var $target TargetModel */
        $target = TargetModel::find()
                ->where([
                    'id' => $data['id'],
                    'user_ref' => SiteController::$user->id
                ])
                ->one();
        if(empty($target)){
            return ['error'=>'Not found'];
        }else{
            $target->parent_ref = $data['parent_ref'];
            $target->deadline = $data['deadline'];
            $target->description = $data['description'];
            $target->target_name = $data['target_name'];
            if($target->save()){
                return ['target'=>$target->info];
            }
                
        }
    }

}
