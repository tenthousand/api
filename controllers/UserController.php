<?php

/**
 * Контроллер пользователей.
 *
 * @author Виктор
 */

namespace app\controllers;

use app\components\Request;
use app\models\UserModel;
use yii\web\Controller;

class UserController extends Controller {
    
    public function beforeAction($action) {
        return true;
    }

    public function actionLogin($data){
        return UserModel::login($data);
    }

}
