<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use app\models\DoingModel;

/**
 * Description of DoingController
 *
 * @author Виктор
 */
class DoingController {

    public function actionCreate($data) {
        $doing = DoingModel::create($data);
        return ['id' => $doing->id];
    }

    public function actionDelete($data) {
        $doing = DoingModel::findOne($data['id']);
        if (!empty($doing)) {
            if (!$doing->checkUser()) {
                return ['error' => 'Forbidden'];
            }
            $doing->delete();
        }
        return ['result' => 'Ok'];
    }

    public function actionEdit($data) {
        $doing = DoingModel::findOne($data['id']);
        if (!empty($doing)) {
            if (!$doing->checkUser()) {
                return ['error' => 'Forbidden'];
            }
            $doing->edit($data);
            $doing->save();
            return $doing->info;
        }else{
            return ['error'=>'not found'];
        }
    }
    
    public function actionGet($data) {
        $doing = DoingModel::findOne($data['id']);
        if(empty($doing)){
            return ['error'=>'not found'];
        }
            if (!$doing->checkUser()) {
                return ['error' => 'Forbidden'];
            }
        return $doing->info;
    }

}
