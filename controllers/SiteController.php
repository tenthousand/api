<?php

namespace app\controllers;

require_once __DIR__. '/../components/Encryption/Jwt.php';

use app\components\Jwt;
use app\components\Request;
use app\components\Utility;
use app\models\UserModel;
use Yii;
use yii\db\Exception;
use yii\web\Controller;

class SiteController extends Controller {

    /**
     *
     * @var UserModel $user
     */
    public static $user;
//
//    /**
//     * @inheritdoc
//     */
//    public function behaviors() {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout'],
//                'rules' => [
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }
//
    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            
//            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
//            ],
        ];
    }
    
    public function beforeAction($action) {
        if(in_array($action->id, ['facade'])){
            $this->enableCsrfValidation = FALSE;
        }
        return parent::beforeAction($action);
    }

//    /**
//     * Displays homepage.
//     *
//     * @return string
//     */
//    public function actionIndex() {
//        return $this->render('index');
//    }
//
//    /**
//     * Login action.
//     *
//     * @return string
//     */
//    public function actionLogin() {
//        if (!Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        }
//        return $this->render('login', [
//                    'model' => $model,
//        ]);
//    }
//
//    /**
//     * Logout action.
//     *
//     * @return string
//     */
//    public function actionLogout() {
//        Yii::$app->user->logout();
//
//        return $this->goHome();
//    }
//
//    /**
//     * Displays contact page.
//     *
//     * @return string
//     */
//    public function actionContact() {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//                    'model' => $model,
//        ]);
//    }
//
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

    public function actionPhpinfo() {
        return phpinfo();
    }

    public function actionTest() {
        return $this->render('test');
    }

    public function actionFacade() {
        try {
            $request = Request::createFromGlobals();
            if ($request->server['REQUEST_METHOD'] == 'OPTIONS') {
                return;
            }
            $action = $request->request;
            if(empty($action['method'])){
                return $this->actionAbout();
            }
            if ($action['method'] != 'user/login') {
                if (!isset($request->headers['AUTHORIZATION'])) {
                    $error = [403, 'Not authorized'];
                } else {
                    $token = $request->headers['AUTHORIZATION'];
                    $payload = (new Jwt())->decode($token, Utility::getAppWeb()['token_secret']);
                    if (empty($payload)) {
                        $error = [403, 'Not authorized'];
                    } else {
                        if ($payload['exp'] < time()) {
                            $error = [408, 'Not authorized'];
                        }
                    }
                    if (!empty($error)) {
                        Yii::$app->response->setStatusCode($error[0]);
                        return json_encode(['error' => $error]);
                    }
                    self::$user = UserModel::getUserFromPayload($payload);
                }
            }
            $answer = $this->controllerAction($action);
            return json_encode($answer);
        } catch (Exception $ex) {
//            Yii::$app->response->setStatusCode(500);
            return json_encode(['error' => $ex->getMessage()]);
        }
    }

    /**
     * Принимает одно событие с клиента и отправляет в нужный контроллер
     * @param $action
     * @return string
     */
    public function controllerAction($action) {
        // Формирование строки имени класса
        $globalString = 'app\controllers\\';
        $names = explode('/', $action['method']);
        $controllerClassName = $globalString . ucfirst($names[0]) . 'Controller';
        $actionName = 'action' . ucfirst($names[1]);
        // Вызов необходимого контроллера
        if(class_exists($controllerClassName)){
            $controller = new $controllerClassName($names[0], '');
        }else{
            return ['error' => $controllerClassName.' not found'];
        }
        return $controller->$actionName($action['body']);
    }

    public static function setUser($user){
        self::$user = $user;
    }
}
