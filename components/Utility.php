<?php

/*
 * Created by NetBeans.
 * User: vukarev
 * Date: 19/01/2016
 * Time: 09:52
 */

namespace app\components;

class Utility {

    public function divideIntToArray($value, $num) {
        $dim = [];
        if ($num > 0) {
            $part = $value / $num;
            for ($i = 1; $i < $num; $i++) {
                $dim[] = intval($part * $i);
            }
            $dim[] = $value; //последняя - точно равна $value
            for ($i = $num - 1; $i > 0; $i--) {
                $dim[$i] -= $dim[$i - 1];
            }
        }
        return $dim;
    }
    /**
     * Возвращает GUID
     * @return string
     */
    public static function guid()
    {
	if(function_exists('com_create_guid'))
	{
	    return com_create_guid();
	}
	else
	{
	    mt_srand((double)microtime()*10000);
	    $charid = md5(uniqid(rand(), true));
	    $hyphen = chr(45);// "-"
	    $uuid = substr($charid, 0, 8).$hyphen
		.substr($charid, 8, 4).$hyphen
		.substr($charid,12, 4).$hyphen
		.substr($charid,16, 4).$hyphen
		.substr($charid,20,12);
	    return $uuid;
	}
    }
    /**
     * Текущая метка времени в миллисекундах
     * @return float
     */
    public function msTime()
    {
        $ms = microtime(true);
        $ms = $ms * 1000;
        return round($ms);
    }

    /**
     * Возвращает Хеш
     * @return string
     */
    public function hash()
    {
       return md5(rand()) . '_' . md5(rand());
    }

    public function deleteOldFiles($dirName){
        $directory = dir($dirName);
        if(empty($directory)){
            return;
        }
        while (false !== ($entry = $directory->read())) {
            if(is_file($entry) && (fileatime($entry) < date('U')-3600*24)){
                unlink($dirName.$entry);
            }
        }
    }

    public static function getAppWeb() {
        $appFile = "app_secret.json";
        $file = __DIR__ . '/../config/' . $appFile;
        if (file_exists($file)) {
            $web = json_decode(file_get_contents($file), true)['web'];
            return $web;
        } else {
            return [];
        }
    }

    public static function getPassKey(){
        $appFile = "app_secret.json";
        $file = __DIR__ . '/../config/' . $appFile;
        if (file_exists($file)) {
            $pass_key = json_decode(file_get_contents($file), true)['pass_key'];
            return $pass_key;
        } else {
            return '';
        }
    }

    public static function generateRandomString($length = 10) {
        $characters = '01234567890123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Получить ошибки верификации в виде строки
     * @param ActiveRecord $record
     */
    public static function stringError($record) {
        $errors = $record->getErrors();
        $str = '';
        foreach ($errors as $key=>$messages) {
            if(!empty($str)){
                $str .= '; ';
            }
            $str .= $key.': '.  implode(',', $messages);
        }
        return $str;
    }

}
