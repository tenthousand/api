<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script>
    var token;
    
    function sendLogin() {
        $.ajax({
            method: "POST",
            url: "/index.php/site/facade",
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify({
                method: "user/login",
                body: {
                    password: $('#password').val(),
                    email: $('#email').val()
                }}),
            success: function (dataR) {
                dataR = JSON.parse(dataR);
                token = dataR.token;
                console.log(dataR);
            }
        });
    }
    function getTargets() {
        $.ajax({
            method: "POST",
            url: "/index.php/site/facade",
            contentType: 'application/json; charset=UTF-8',
            data: JSON.stringify({
                method: "target/list",
                body: {
                }}),
            beforeSend: function (request)
            {
                request.setRequestHeader("Authorization", token);
            },
            success: function (dataR) {
                dataR = JSON.parse(dataR);
                console.log(dataR);
            }

        });
    }
</script>
<div style="background-color: #ccc">
    <label for="username">User Name: <input name="username" id="username" type="text"/></label><br/>
    <label for="email">Email: <input name="email" id="email" type="text"/></label><br/>
    <label for="password">Password: <input name="password" id="password" type="text"/></label><br/>
    <div>
        <button onclick="sendLogin();">Login</button> <button onclick="getTargets();">getTargets</button>
    </div>
</div>
